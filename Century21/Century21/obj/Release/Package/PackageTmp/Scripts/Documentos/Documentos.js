﻿



var Documentos = (function () {
    var server = 'https://centurybe.rosolutions.com.mx',
        user = "",
        objFull = [],
        id = null;

    //function Get() { };

    return {
        Get: function () {
            $.ajax({
                type: "GET",
                url: server + '/documents/search',
                headers: {
                    "X-Api-Token": xTok
                },
                //data: {
                //    'user': userid
                //},
                success: function (data) {
                    console.log(data);
                    objFull = data.rows;
                    Documentos.LoadTable(data.rows);
                }

            })
                .fail(function (data) {
                    console.error("No se pueden obtener documentos", data);
                });
        },
        LoadTable: function (json) {
            $("#tblDoctos").DataTable({
                destroy: true,
                info: false,
                searching: true,
                "paging": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "aaData": json,
                "aoColumns": [
                    {
                        "mData": "createdAt",
                        "mRender": function (data) {
                            return moment(data).format("DD/MMM/YYYY hh:mm a");
                        }
                    },
                    { "mData": "originalFile" },
                    {
                        "mRender": function (data, type, full) {
                            var idx = objFull.indexOf(full);
                            var btns = '<button title="DESCARGAR" data-toogle="tooltip" data-placement="left" class="btn btn-success" onclick="Documentos.Download(' + idx + ');"><i class="fas fa-download"></i></button> ';
                            if (userType == 1) {
                                btns += ' <button title="ELIMINAR" class="btn btn-danger" onclick="Documentos.ModalDelete(' + idx + ');"><i class="fas fa-times"></i></button>';
                            }
                            return btns;
                        }, "sClass": "tar"
                    }
                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "zeroRecords": "No se encontró nada - lo sentimos",
                    "search": "Buscar en tabla:",
                    "emptyTable": "No hay datos disponibles",
                    "infoEmpty": "No se encuentra información disponible",
                    "infoFiltered": "(filtrado de _MAX_ items totales)",
                    "paginate":
                    {
                        "next": '<i class="fas fa-angle-double-right"></i>',
                        "previous": '<i class="fas fa-angle-double-left"></i>'
                    }
                }
            });
        },
        ModalSubir: function () {
            $("#modal-subir").modal("show");
        },
        ModalDelete: function (idx) {
            var doc = objFull[idx];
            id = doc.id;
            console.log("modalDelete", doc);
            $("#docName").text(doc.originalFile);
            $("#modal-delete").modal("show");
        },
        Upload: function () {
            RunLoader(".contenedor");
            var fileToSend = new FormData();
            fileToSend.append("document", $("#inp-file")[0].files[0])
            $.ajax({
                method: "POST",
                url: server + "/documents/",
                headers: {
                    "X-Api-Token": xTok,
                },
                contentType: false,
                processData: false,
                data: fileToSend,
                success: function (data) {
                    console.log("se agregó correctamente");
                    Documentos.Get();
                    StopLoader('.contenedor');
                    $("#modal-subir").modal("hide");
                }
            })
                .fail(function () {

                    $("#modal-subir").modal("hide");
                    StopLoader('.contenedor');
                })

        },
        Download: function (idx) {
            var doc = objFull[idx];
            var pk = doc.id;
            $.ajax({
                type: "GET",
                url: server + "/documents/" + pk,
                headers: {
                    'X-Api-Token':xTok
                },
                success: function (data) {
                    Documentos.DownloadSecure(pk, data.token);
                }
            })
                .fail(function (data) {
                    console.error("error", data)
                });
        },
        DownloadSecure: function (pk, token) {
            var url= server + "/documents/" + pk + "/download?token=" + token;
            window.open(url,"_blank");
        },
        Delete: function () {
            $.ajax({
                type: "DELETE",
                url: server + "/documents/" + id,
                headers: {
                    "X-Api-Token": xTok,
                },
                success: function (data) {
                    console.log("se eliminó", data);
                    Documentos.Get();
                    $("#modal-delete").modal('hide');
                  
                }
            })
                .fail(function (data) {
                    console.error("fail", data);
                    $("#modal-delete").modal('hide');
                })
        },
        Procesar: function (evt) {
            var limite = 5242880;
            console.log("archivo reader");
            var files = evt.target.files; // FileList object
            f = files[0];
            
            var reader = new FileReader();

            reader.onload = (function (theFile) {

                if (theFile.size>limite) {
                    //notification de error
                    console.error("archivo muy grande");
                }
                else {
                    return function (e) {
                        console.log("onload");
                        var name = document.getElementById('inp-file').files[0].name;
                        $("#fileName").text(name);
                    };
                }
                
            })(f);

            reader.readAsDataURL(f);

        },
        Init: function () {
            if (userType == 1) {
                var btnsubir = document.getElementById('btnSubir'),
                    btnSmtSubir = document.getElementById('btnSmtSubir'),
                    btnDel = document.getElementById('btnDelDoc');

                btnsubir.addEventListener("click", Documentos.ModalSubir);
                btnSmtSubir.addEventListener("click", Documentos.Upload);
                btnDel.addEventListener("click", Documentos.Delete);
                document.getElementById('inp-file').addEventListener('change', Documentos.Procesar, false);

                $("#modal-subir").on("hidden.bs.modal", function () {
                    document.getElementById('fileName').innerText = "";
                    $("#formUpload")[0].reset();
                });

                $("#modal-delete").on("hidden.bs.modal", function () {
                    id = null;
                });
            }

            Documentos.Get();
        }

    }

})();

(function () {
    Documentos.Init();
})();