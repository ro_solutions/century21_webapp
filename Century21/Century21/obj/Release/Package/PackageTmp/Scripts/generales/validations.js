﻿Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

Array.prototype.orderByNumber = function (p, so) {
    if (so != -1 && so != 1) so = 1;
    this.sort(function (a, b) {
        return (a[p] - b[p]) * so;
    })
}


function RunLoader(element) {
    console.log("RunLoaded");
    $(element).waitMe({
        effect: 'bounce',
        text: '',
        bg: 'rgba(255, 255, 255, 0.7)',
        color: '#000',
        maxSize: '',
        waitTime: -1,
        textPos: 'vertical',
        fontSize: '',
    });
};

function StopLoader(element) {
    $(element).waitMe("hide");
};