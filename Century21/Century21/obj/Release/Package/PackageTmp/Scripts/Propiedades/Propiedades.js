﻿function isNull(value, val) {
    return value == null ? val : value;
}
function GetValue(id) {
    return document.getElementById(id).value;
};
function SetValue(id, value) {
    return document.getElementById(id).value = value;
};
function SetRadio(tagName, value) {
    var radios = document.getElementsByName(tagName);
    for (var i = 0; i < radios.length; i += 1) {
        if (radios[i].value == value) {
            radios[i].checked = true;
        }
    }
};
var Generales = (function () {
    var server = 'https://centurybe.rosolutions.com.mx',
        xToken = xTok;
    return {
        GetClientes: function () {
            $.ajax({
                type: "GET",
                url: server + "/clients/search/",
                headers: {
                    "X-Api-Token": xToken
                },
                success: function (data) {
                    Generales.LoadSelectCliente(data.rows, "Seleccione Cliente");
                }
            }).fail(function (data) {
                console.log("No se pueden cargar los clientes", data);
            });
        },
        GetAsesores: function () {
            $.ajax({
                type: "GET",
                url: server + "/users/search/",
                headers: {
                    "X-Api-Token": xToken
                },
                success: function (data) {
                    Generales.LoadSelectAsesor(data.rows, "Seleccione asesor");
                },
                fail: function (data) {
                    console.log("done", data);
                }
            });
        },
        LoadSelectAsesor: function (json, placeholder) {
            var html = '<option></option>';
            for (var i = 0; i < json.length; i++) {
                html += '<option value="' + json[i].user_id + '">' + json[i].nombres + ' ' + json[i].apellidos + '</option>';
            }
            $("#inpAsesor").empty().append(html);
            $("#inpAsesor").select2({
                width: 'resolve',
                heigth: "resolve",
                placeholder: placeholder,
                allowClear: true,

            });
        },
        LoadSelectCliente: function (json, placeholder) {
            var html = '<option></option>';
            for (var i = 0; i < json.length; i++) {
                html += '<option value="' + json[i].client_id + '">' + json[i].nombres + ' ' + json[i].apellidos + '</option>';
            }
            $("#inpCliente").empty().append(html);
            $("#inpCliente").select2({
                width: 'resolve',
                heigth: "resolve",
                placeholder: placeholder,
                allowClear: true,

            });
        }
    }
})();

var inicial = {
    lat: 29.0722048,
    lng: -110.9817454
};
function InitMap() {
    var currentMarker;

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: inicial
    });

    function addMarker(location, map) {
        if (currentMarker) {
            currentMarker.setMap(null);
        }

        Propiedades.SetMapValues(location.lat, location.lng);
        //$lat.value = location.lat;
        //$lng.value = location.lng;

        currentMarker = new google.maps.Marker({
            position: location,
            map: map
        });
    }

    addMarker(inicial, map);

    google.maps.event.addListener(map, 'click', function (event) {
        addMarker({
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
        }, map);
    });

};

$('#modal-add').on('shown.bs.modal', function () {
    InitMap();
});
var Propiedades = (function () {
    var server = 'https://centurybe.rosolutions.com.mx',
        serverImgs = server + '/images/',
        xToken = xTok,
        objFull = [],
        pkId = null,
        imgToChange = 0;
    opciones = {
        "mRemder": function () {
            return '';
        }};
    var mapa = { lat: '', long: '' };

    return {
        SetMapValues: function (lat, long) {
            mapa.lat = lat;
            mapa.long = long;
            console.log("mapa", mapa);
        },
        GetPropiedades: function () {
            RunLoader(".contenedor");
            $.ajax({
                type: "GET",
                url: server + "/properties/search",
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    clave: GetValue("inpFiltroClave"),
                    precioMenor: GetValue("menorQue"),
                    precioMayor: GetValue("mayorQue"),
                },
                success: function (data) {
                    StopLoader(".contenedor");
                    Propiedades.LoadTable(data.rows);
                    $('[data-toggle="tooltip"]').tooltip();
                }
            })
                .fail(function (data) {
                    console.error("No se pueden cargar las propiedades", data);
                    StopLoader('.contenedor');
                });
        },
        //Carga la tabla principal de propiedades
        LoadTable: function (json) {
            objFull = json;
            $("#tblPropiedades").DataTable({
                destroy: true,
                info: false,
                searching: true,
                "paging": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "aaData": json,
                "aoColumns": [
                    {
                        "mData": "createdAt",
                        "mRender": function (data) {
                            
                            return moment(data).format("DD/MMM/YYYY hh:mm a");
                        }
                    },
                    { "mData": "clave" },
                    {
                        "mData": "contract_type",
                        "mRender": function (data) {
                            if (data) {
                                return "Venta";
                            }
                            else {
                                return "Renta";
                            }
                        }
                    },
                    {
                        "mRender": function (data, type, full) {
                            var direccion = '<div class="col-lg-12 col-xs-12"><strong> Calle: </strong>' + isNull(full.direccionCalle, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>Col: </strong>' + isNull(full.direccionColonia, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>Num Ext: </strong>' + isNull(full.direccionExterior, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>Num Int: </strong>' + isNull(full.direccionInterior, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>CP: </strong>' + isNull(full.cp, "") + '</div>';

                            return direccion;
                        }
                    },
                    {
                        "mData": "type",
                        "mRender": function (data) {
                            var tipo = data;
                            switch (tipo) {
                                case 1:
                                    tipo = "Residencial";
                                    break;
                                case 2:
                                    tipo = "Comercial";
                                    break;
                                case 3:
                                    tipo = "Industrial";
                                    break;
                                case 4:
                                    tipo = "Desarrollo";
                                    break;
                                case 5:
                                    tipo = "Playas";
                                    break;
                                case 6:
                                    tipo = "Campestre";
                                    break;
                                default:
                                    tipo = "ha ocurrido un error";
                                    break;
                            }
                            return tipo;
                        }
                    },
                    {
                        "mData": "user",
                        "mRender": function (data) {
                            return data.nombres + " " + data.apellidos;
                        }//cambiar por nombre del asesor
                    },
                    {
                        "mData": "precio",
                        "mRender": function (data) {
                            return "$" + parseFloat(data).format(2);
                        }, "sClass": "tar"
                    },
                    {
                        "mData": "status",
                        "mRender": function (data) {
                            var tipo = '';
                            switch (data) {
                                case 1:
                                    tipo = "Alta";
                                    break;
                                case 2:
                                    tipo = "Baja por Venta";
                                    break;
                                case 3:
                                    tipo = "Baja por Rente";
                                    break;
                                case 4:
                                    tipo = "Baja";
                                    break;
                                default:
                                    tipo = "";
                                    break;
                            }
                            return tipo;
                        }
                    },
                    {
                        "mData": "residenciaMes",
                        "mRender": function (data, type, full) {
                            var idx = objFull.indexOf(full);

                            if (typeUser ==1) {
                                if (data == true) {
                                    return ' <button title="Quitar como propiedad del mes" data-toggle="tooltip" data-placement="right" class="btn btn-danger btn-sm" onclick="Propiedades.QuitMes(' + idx + ')"><i class="fas fa-times"></i></button>';
                                }
                                else {
                                    return '<button title="Asignar como Propiedad del mes" data-toggle="tooltip" data-placement="right" class="btn btn-success success-color btn-control btn-sm" onclick="Propiedades.SetMes(' + idx + ');"><i class="fas fa-check"></i></button>'
                                }
                            }
                            else {
                                if (data == true) {
                                    return '<div title="Propiedad del mes" data-toggle="tooltip" data-placement="right" class="btn btn-success btn-sm" style="color:#fff; cursor:default;"><i class="fas fa-check"></i></div>'
                                }
                                else {
                                    return '';
                                }
                            }
                            

                        }, "sClass": "tac"
                    },
                    
                    opciones

                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "zeroRecords": "No se encontró nada - lo sentimos",
                    "search": "Buscar:",
                    "emptyTable": "No hay datos disponibles",
                    "infoEmpty": "No se encuentra información disponible",
                    "infoFiltered": "(filtrado de _MAX_ items totales)",
                    "paginate":
                    {
                        "next": '<i class="fas fa-angle-double-right"></i>',
                        "previous": '<i class="fas fa-angle-double-left"></i>'
                    }
                }

            });
        },
        //Asigna la propiedad del mes
        SetMes: function (idx) {
            var p = objFull[idx];

            $.ajax({
                type: "PUT",
                url: server + "/properties/" + p.property_id,
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    'residenciaMes': true
                },
                success: function (data) {
                    Propiedades.GetPropiedades();
                    $(".modal").modal("hide");
                }
            }).fail(function (data) {
                console.error("fail", data);
            });
        },
        //Quita la propiedad del mes
        QuitMes: function (idx) {
            var p = objFull[idx];
            $.ajax({
                type: "PUT",
                url: server + "/properties/" + p.property_id,
                headers: {
                    "X-Api-Token": xToken
                },
                data: JSON.stringify({
                    'residenciaMes': false
                }),
                contentType: "application/json",
                success: function (data) {
                    Propiedades.GetPropiedades();
                    //$(".modal").modal("hide");
                }
            })
                .fail(function (data) {
                    console.error("fail", data);
                });
        },
        //Valida campos antes de agregar o editar una propiedad
        Validar: function myfunction() {
            $("#formPropiedad").validate({
                focusCleanup: true,
                rules: {
                    inpClave: { required: true },
                    inpTipo: { required: true },
                    //inpPrecio: { required: true },
                    inpCliente: { required: true },
                    //inpZona: { required: true },
                    //inpCalle: { required: true },
                    //inpColonia: { required: true },
                    //inpCiudad: { required: true },
                    //inpEstado: { required: true },
                    //inpTerreno: { required: true },
                    //inpConstruccion: { required: true },
                    inpAsesor: { required: true },
                    inpStatus: { required: true },
                    ventarenta: { required: true }
                },
                messages: {
                    inpClave: { required: "Campo Requerido" },
                    inpTipo: { required: "Campo Requerido" },
                    //inpPrecio: { required: "Campo Requerido" },
                    inpCliente: { required: "Campo Requerido" },
                    //inpZona: { required: "Campo Requerido" },
                    //inpCalle: { required: "Campo Requerido" },
                    //inpColonia: { required: "Campo Requerido" },
                    //inpCiudad: { required: "Campo Requerido" },
                    //inpEstado: { required: "Campo Requerido" },
                    //inpTerreno: { required: "Campo Requerido" },
                    //inpConstruccion: { required: "Campo Requerido" },
                    inpAsesor: { required: "Campo Requerido" },
                    inpStatus: { required: "Campo Requerido" },
                    ventarenta: { required: "Campo Requerido" }
                },
                submitHandler: function (form) {
                    objAction[action]();
                    return false;
                }
            });
        },
        //POST para agregar una propiedad
        Add: function () {
            $.ajax({
                type: "POST",
                url: server + "/properties/",
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "user_id": $("#inpAsesor").val(),
                    "client_id": $("#inpCliente").val(),
                    "clave": GetValue("inpClave"),
                    "type": $("#inpTipo").val(),
                    "contract_type": $('input[name=ventarenta]:checked').val() == "Venta" ? true : false,
                    "precio": GetValue("inpPrecio"),
                    "latitud": mapa.lat,
                    "longitud": mapa.long,
                    "zona": GetValue("inpZona"),
                    "direccionCalle": GetValue("inpCalle"),
                    "direccionColonia": GetValue("inpColonia"),
                    "direccionExterior": parseInt(GetValue("inpExterior")),
                    "direccionInterior": parseInt(GetValue("inpInterior")),
                    "cp": GetValue("inpCP"),
                    "ciudad": GetValue("inpCiudad"),
                    "estado": parseInt($("#inpEstado").val()),
                    "terreno": parseInt(GetValue("inpTerreno")),
                    "construccion": parseInt(GetValue("inpConstruccion")),
                    "recamaras": parseInt(GetValue("inpRecamara")),
                    "bathrooms": parseInt(GetValue("inpBanos")),
                    "cochera": parseInt(GetValue("inpCochera")),
                    "edad": parseInt(GetValue("inpEdad")),
                    "estancia": $('input[name=inpEstancia]:checked').val() == "si" ? true : false,
                    "privada": $('input[name=inpPrivada]:checked').val() == "si" ? true : false,
                    "status": $("#inpStatus").val(),
                    "description": GetValue('inpDescription')

                },
                success: function (data) {
                    Propiedades.GetPropiedades();
                    $(".modal").modal("hide");
                }
            }).fail(function (data) {
                console.error("fail", data);
            });
        },
        //POST para editar una propiedad
        Edit: function () {
            $.ajax({
                type: "PUT",
                url: server + "/properties/" + pkId,
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "user_id": $("#inpAsesor").val(),
                    "client_id": $("#inpCliente").val(),
                    "clave": GetValue("inpClave"),
                    "type": $("#inpTipo").val(),
                    "contract_type": $('input[name=ventarenta]:checked').val() == "Venta" ? true : false,
                    "precio": GetValue("inpPrecio"),
                    "latitud": mapa.lat,
                    "longitud": mapa.long,
                    "zona": GetValue("inpZona"),
                    "direccionCalle": GetValue("inpCalle"),
                    "direccionColonia": GetValue("inpColonia"),
                    "direccionExterior": parseInt(GetValue("inpExterior")),
                    "direccionInterior": parseInt(GetValue("inpInterior")),
                    "cp": GetValue("inpCP"),
                    "ciudad": GetValue("inpCiudad"),
                    "estado": parseInt($("#inpEstado").val()),
                    "terreno": parseInt(GetValue("inpTerreno")),
                    "construccion": parseInt(GetValue("inpConstruccion")),
                    "recamaras": parseInt(GetValue("inpRecamara")),
                    "bathrooms": parseInt(GetValue("inpBanos")),
                    "cochera": parseInt(GetValue("inpCochera")),
                    "edad": parseInt(GetValue("inpEdad")),
                    "estancia": $('input[name=inpEstancia]:checked').val() == "si" ? true : false,
                    "privada": $('input[name=inpPrivada]:checked').val() == "si" ? true : false,
                    "status": $("#inpStatus").val(),
                    "description": GetValue('inpDescription')
                },
                success: function (data) {
                    Propiedades.GetPropiedades();
                    $(".modal").modal("hide");
                }
            }).fail(function (data) {
                console.error("fail", data);
            });
        },
        //Mostrar el detalle técnico de una propiedad
        DetalleTecnico: function () {

        },
        //Mostrar detalle cualitativo
        DetalleCualitativo: function () {

        },
        //Mostrar Valor Comercial
        ValorComercial: function () {

        },
        // abre el modal para agregar propiedades
        modalAdd: function () {
            inicial = {
                lat: 29.083026364757238,
                lng: -110.95466136932373
            }
            action = actions[0];
            document.getElementById('titleProp').innerText = 'Agregar Propiedad';
            document.getElementById('btnSubmit').innerText = 'Agregar';
            $("#modal-add").modal("show");
        },
        //abre modal add/edit con datos de la propiedad seleccionada
        modalEdit: function (idx) {
            var propiedad = objFull[idx];
            pkId = propiedad.property_id;
            action = actions[1];
            //set Object to inputs
            inicial.lat = typeof (propiedad.latitud) == "number" ? propiedad.latitud : inicial.lat;
            inicial.lng = typeof (propiedad.longitud) == "number" ? propiedad.longitud : inicial.lng;


            SetValue('inpClave', propiedad.clave);
            $("#inpTipo").val(propiedad.type)
            SetValue('inpDescription', propiedad.description);
            SetRadio("ventarenta", propiedad.contract_type ? "Venta" : "Renta");
            SetValue('inpPrecio', propiedad.precio);
            $("#inpCliente").val(propiedad.client_id).trigger('change.select2');
            SetValue('inpZona', propiedad.zona);
            SetValue('inpCalle', propiedad.direccionCalle);
            SetValue('inpInterior', propiedad.direccionInterior);
            SetValue('inpExterior', propiedad.direccionExterior);
            SetValue('inpCP', propiedad.cp);
            SetValue('inpColonia', propiedad.direccionColonia);
            $("#inpEstado").val(propiedad.estado).trigger('change.select2');
            SetValue('inpTerreno', propiedad.terreno);
            SetValue('inpConstruccion', propiedad.construccion);
            SetValue('inpRecamara', propiedad.recamaras);
            SetValue('inpBanos', propiedad.bathrooms);
            SetValue('inpCochera', propiedad.cochera);
            SetValue('inpEdad', propiedad.edad);
            SetValue("inpCiudad", propiedad.ciudad);

            SetRadio("inpEstancia", propiedad.estancia ? "si" : "no");
            SetRadio("inpPrivada", propiedad.privada ? "si" : "no");

            $("#inpAsesor").val(propiedad.user_id).trigger('change.select2');
            //end Set

            document.getElementById('titleProp').innerText = 'Editar Propiedad';
            document.getElementById('btnSubmit').innerText = 'Editar';
            $("#modal-add").modal("show");
        },
        //Se abre al dar click en el boton con icono de cámara 
        //para editar las imagenes de la propiedad
        modalImgs: function (idx) {
            var p = objFull[idx];
            pkId = p.property_id;
            $.ajax({
                type: "GET",
                url: server + '/properties/' + pkId + '/images',
                success: function (data) {
                    console.log("data img", data);
                    Propiedades.FillImg(data);
                    $("#modal-imgs").modal("show");
                }

            })
                .fail(function (data) {
                    console.error("fail", data);
                });
        },
        //llena los campos de imágenes de propiedad en el modal de imágenes
        FillImg: function (json) {
            var img = document.querySelectorAll(".image");

            img.forEach(i => $(i).attr("src", "/Content/imgs/63272.svg"))

            for (var i = 0; i < json.length; i++) {
                $("#imgProp" + (i + 1)).attr("data-img", serverImgs + json[i].id);
                $("#imgProp" + (i + 1)).attr("src", serverImgs + json[i].image);

            }
            $("#modal-imgs").modal("show");
        },

        //POST para agregar una imagen 
        SetImg: function (src) {
            console.log("imgTochange", imgToChange);
            console.log("set picture put");
            Propiedades.DelImg(imgToChange);
            var dataToSend = new FormData();

            dataToSend.append('image', $('#imgSelector')[0].files[0]);

            var url = server + "/properties/" + pkId + "/images";
            console.log("url", url);
            $.ajax({
                method: "POST",
                url: url,
                headers: {
                    "X-Api-Token": xToken,
                },
                contentType: false,
                processData: false,
                data: dataToSend,
                success: function (data) {
                    console.log("success propertie image loaded");

                    $("#imgProp" + imgToChange).attr("src", src);
                    //$(".modal").modal("hide");
                    //Asesores.GetAsesores();
                }
            })
                .fail(function (data) {
                    console.log("fail", data);
                });
        },
        //img id de input 
        SetIdImg: function () {
            imgToChange = this.dataset.img;
            console.log("img", this.dataset.img);
            document.getElementById("imgSelector").click();
        },
        
        ReadImg: function (evt) {
            console.log("archivo reader");
            var files = evt.target.files; // FileList object

            //Obtenemos la imagen del campo "file". 
            for (var i = 0, f; f = files[i]; i++) {
                //Solo admitimos imágenes.
                if (!f.type.match('image.*')) {
                    continue;
                }

                var reader = new FileReader();

                reader.onload = (function (theFile) {
                    //clousure
                    return function (e) {
                        console.log("onload");
                        //hay que guardarla también
                        Propiedades.SetImg(e.target.result);


                    };
                })(f);

                reader.readAsDataURL(f);
            }
        },

        DelImg: function (idImg) {
            $.ajax({
                type: "DELETE",
                url: server + "/properties/" + pkId + "/images/" + idImg,
                headers: {
                    "X-Api-Token": xToken,
                },
                success: function (data) {
                    console.log("Se ha eliminado la imagen ");
                    //$(".modal").modal("hide");
                    //Asesores.GetAsesores();
                }
            })
        },

        Init: function () {

            Propiedades.GetPropiedades();
            Propiedades.Validar();

            var btnBusqueda = document.getElementById("filterBtn");
            btnBusqueda.addEventListener("click", Propiedades.GetPropiedades);

            if (typeUser == 1) {

                opciones =
                    {
                    "mRender": function (data, type, full) {
                        var idx = objFull.indexOf(full);
                        var btns = '<button class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="Editar" onclick="Propiedades.modalEdit(' + idx + ');"><i class="fas fa-edit"></i></button> ' +
                            ' <button class="btn btn-sm btn-info" onclick="Propiedades.modalImgs(' + idx + ');"><i class="fas fa-camera"></i></button>';
                        return btns;
                    },
                    "sClass": "tac"
                }

                var inpFiles = document.getElementById("imgSelector"),
                    modal = document.getElementById("btnAdd"),
                    btns = document.querySelectorAll(".overlay");

                modal.addEventListener("click", Propiedades.modalAdd);
                inpFiles.addEventListener("change", Propiedades.ReadImg);
                btns.forEach(div => div.addEventListener("click", Propiedades.SetIdImg));
                Generales.GetClientes();
            }
            else {
                opciones = {
                    "mRender": function (data, type, full) {
                        var idx = objFull.indexOf(full);
                        var btns = '<button class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="left" title="Enviar Información" onclick="Propiedades.modalEnviar(' + idx + ');"><i class="fas fa-envelope"></i></button> ';
                        return btns;
                    },
                    "sClass": "tac"
                }
            }

            Generales.GetAsesores();
            
            $("#modal-add").on("hide.bs.modal", function () {
                $("#formPropiedad").validate().resetForm();
                $("#formPropiedad")[0].reset();
            });

            $("#inpEstado").select2({
                width: 'resolve',
                placeholder: 'Ingrese estado',
                allowClear: true,

            });

        }

    }
})();

const actions = ["add", "edit"];
var action = actions[0],
    objAction = { "add": Propiedades.Add, "edit": Propiedades.Edit };
(function () {
    Propiedades.Init();
})();
