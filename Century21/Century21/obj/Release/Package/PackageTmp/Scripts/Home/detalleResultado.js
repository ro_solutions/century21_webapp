﻿
var changeType = function (value) {
    var tipo = "";
    switch (value) {
        case 1:
            tipo = "Residencial";
            break;
        case 2:
            tipo = "Comercial";
            break;
        case 3:
            tipo = "Industrial";
            break;
        case 4:
            tipo = "Desarrollo";
            break;
        case 5:
            tipo = "Playas";
            break;
        case 6:
            tipo = "Campestre";
            break;
        default:
            tipo = "ha ocurrido un error";
            break;
    }
    return tipo;
}
var GetEstado = function (edo) {
    var estado = ""

    switch (edo) {
        case 1: estado = "Aguascalientes"; break;
        case 2: estado = "Baja California"; break;
        case 3: estado = "Baja California Sur"; break;
        case 4: estado = "Campeche"; break;
        case 5: estado = "Chiapas"; break;
        case 6: estado = "Chihuahua"; break;
        case 7: estado = "Ciudad de México"; break;
        case 8: estado = "Coahuila"; break;
        case 9: estado = "Colima"; break;
        case 10: estado = "Durango"; break;
        case 11: estado = "Estado de México"; break;
        case 12: estado = "Guanajuato"; break;
        case 13: estado = "Guerrero"; break;
        case 14: estado = "Hidalgo"; break;
        case 15: estado = "Jalisco"; break;
        case 16: estado = "Michoacán"; break;
        case 17: estado = "Morelos"; break;
        case 18: estado = "Nayarit"; break;
        case 19: estado = "Nuevo León"; break;
        case 20: estado = "Oaxaca"; break;
        case 21: estado = "Puebla"; break;
        case 22: estado = "Querétaro"; break;
        case 23: estado = "Quintana Roo"; break;
        case 24: estado = "San Luis Potosí"; break;
        case 25: estado = "Sinaloa"; break;
        case 26: estado = "Sonora"; break;
        case 27: estado = "Tabasco"; break;
        case 28: estado = "Tamaulipas"; break;
        case 29: estado = "Tlaxcala"; break;
        case 30: estado = "Veracruz"; break;
        case 31: estado = "Yucatán"; break;
        case 32: estado = "Zacatecas"; break;

        default:
            break;
    }

    return estado;
}

function InitMap() {
    var currentMarker;
    var home = {
        lat: 29.0722048,
        lng: -110.9817454
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15
    });

    function addMarker(location, map) {
        if (currentMarker) {
            currentMarker.setMap(null);
        }

        currentMarker = new google.maps.Marker({
            position: location,
            map: map
        });
    }

    //addMarker(inicial, map);

    google.maps.event.addListener(map, 'click', function (event) {
        addMarker({
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
        }, map);
    });

};

var Detalle = (function () {
    var server = 'https://centurybe.rosolutions.com.mx',
        serverImgs = server + '/images/',
        propiedad = null,
        pkid = pkId,
        clave ='',
        embedMapURL =
            'https://www.google.com/maps/embed/v1/place?key=AIzaSyCtkmN9cQ8EWl0EVVjlREt4tnliu2vRxTQ&q=';
    return {
        Get: function () {
            $.ajax({
                type: "GET",
                url: server + "/properties/" + pkid,
                success: function (data) {
                    console.log("tenemos propiedad", data);
                    Detalle.SetValues(data);
                }
            }).fail(function (data) {
                console.error(data);
            });
        },
        SetValues: function (p) {
            clave = p.clave;
            document.getElementById('type-p').innerText = changeType(p.type);
            document.getElementById('contract-p').innerText = p.contract_type ? "Venta" : "Renta";
            document.getElementById('terreno-p').innerHTML = p.terreno + ' m<sup>2</sup>';
            document.getElementById('construccion-p').innerHTML = p.construccion + ' m<sup>2</sup>';
            document.getElementById('recamaras-p').innerText = p.recamaras;
            document.getElementById('bano-p').innerText = p.bathrooms;
            document.getElementById('bano-p').innerText = p.estancia ? "Si" : "No";
            document.getElementById('cochera-p').innerText = p.cochera;
            document.getElementById('precio-p').innerText = "$ " + parseFloat(p.precio).format(2) + " MxN";
            document.getElementById('description-p').innerText = p.description;

            //Section Asesor
            document.getElementById('AseName').innerText = p.user.nombres + ' ' + p.user.apellidos;
            document.getElementById('AseTel').innerHTML = '<i class="fas fa-phone"></i>' + p.user.telefono;
            document.getElementById('AseEmail').innerHTML = '<i class="fas fa-envelope"></i>' + p.user.email;
            //try {
            $("#img-asesor").attr("src", server + '/images/' + p.user.image);
            //} catch (e) {
            //    console.log("error al poner la imagen del asesor", e);
            //}


            //section cards
            document.getElementById('crd1').innerHTML = changeType(p.type) + " en " + (p.contract_type ? "Venta" : "Renta") + " - " +
                p.direccionColonia;
            document.getElementById('clave-p').innerText = "Clave: " + p.clave;

            document.getElementById('p-calle').innerText = p.direccionCalle + " ";
            document.getElementById('p-num').innerText = p.direccionExterior + ", ";
            document.getElementById('p-col').innerText = p.direccionColonia + " ";
            document.getElementById('p-cd').innerText = p.ciudad + ", ";
            document.getElementById('p-edo').innerText = GetEstado(p.estado);


            $(".map-frame").attr("src", embedMapURL + p.latitud + ',' + p.longitud);
            //Detalle.InitMap(p.latitud, p.longitud);

        },
        GetImages: function () {
            $.ajax({
                type: "GET",
                url: server + '/properties/' + pkid + '/images',
                success: function (data) {
                    console.log("data img", data);
                    Detalle.SetImages(data);
                    $("#modal-imgs").modal("show");
                }

            })
                .fail(function (data) {
                    console.error("fail", data);
                });
        },
        SetImages: function (json) {
            if (json.length > 0) {
                
                var ols = $(".carousel-indicators"),
                    olsHTML = "",
                    itemsContainer = $(".carousel-inner"),
                    items = "",
                    imgMin1 = $("#divImgMin1"),
                    imgminHTML1 = "",
                    imgMin2 = $("#divImgMin2"),
                    imgminHTML2 = "";
                
                for (var i = 0; i < json.length; i++) {
                    var classActive = i == 0 ? "active" : " ";
                    olsHTML += '<li data-target="#detalle-carousel" data-slide-to="' + i + '" class="' + classActive + '"></li>';

                    items += '<div class="item ' + classActive + '">' +
                                '<div class="image-item-cont">' +
                        '<img data-target="#detalle-carousel" data-slide-to="' + i + '" src="' + serverImgs + json[i].image + '" class="img-item">' +
                                '</div>' +
                                //'<img src="' + serverImgs + json[i].image + '" alt="...">' +
                             '</div >';

                    if (i < 3) {
                        //console.log("entra en" ,i);
                        imgminHTML1 += '<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12 bg-gray mg-T border-mini tac no-pd-LR">' +
                                            '<div class="img-min-cont">'+
                                                '<img data-target="#detalle-carousel" data-slide-to="' + i + '" src="' + serverImgs + json[i].image + '" class="image-min">'+
                                            '</div>'+
                                        '</div>';
                    }
                    else {
                        imgminHTML2 += '<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12 bg-gray mg-T border-mini tac no-pd-LR">' +
                                            '<div class="img-min-cont">' +
                                                '<img data-target="#detalle-carousel" data-slide-to="' + i + '" src="' + serverImgs + json[i].image + '" class="image-min">' +
                                            '</div>' +
                                        '</div>';
                    }

                }

                ols.empty().append(olsHTML);
                itemsContainer.empty().append(items);
                imgMin1.empty().append(imgminHTML1);
                imgMin2.empty().append(imgminHTML2);
                $("#detalle-carousel").carousel();
            }
            else {
                $("#detalle-carousel").empty().append("<h3>No se han asignado imágenes a esta propiedad</h3>");
            }
        },
        ModalSend: function () {
            $("#modal-send-info").modal("show");
        },
        Validar: function () {
            $("#frmSend").validate({
                focusCleanup: true,
                rules: {
                    mailInfo: { required: true, email:true }
                },
                messages: {
                    mailInfo: { required: "Campo Requerido", email:"Ingrese correo válido válido." }
                },
                submitHandler: function (form) {
                    Detalle.SendInfo();
                    return false;
                }
            })
        },
        SendInfo: function () {
            console.log("funciona el submit handler");
            $.ajax({
                type: "POST",
                url: server + "/email/property/" + pkid,
                data: {
                    "email": document.getElementById('mailInfo').value
                },
                success: function (data) {
                    console.log("Se envió correo");
                    $(".modal").modal("hide");
                }
            })
                .fail(function (data) {
                    console.log("Por le momento no se puede enviar correo");
                    $(".modal").modal("hide");
                });
        },
        ValidarMasInfo: function () {
            $("#frmMasInfo").validate({
                focusCleanup: true,
                rules: {
                    cname: { required: true },
                    cmail: { required: true, email: true },
                    cphone: { required: true },
                    cmensaje: { required: true }
                },
                messages: {
                    cname: { required: "Campo Requerido" },
                    cmail: { required: "Campo Requerido", email: "Ingrese Correo Electrónico válido" },
                    cphone: { required: "Campo Requerido" },
                    cmensaje: { required: "Campo Requerido"  }
                },
                submitHandler: function (form) {
                    Detalle.SendMasInfo();
                    return false;
                }
            })
        },
        SendMasInfo: function () {
            console.log("funciona el submit handler");
            $.ajax({
                type: "POST",
                url: server + "/email/contact",
                data: {
                    "nombre": document.getElementById('cname').value,
                    "mensaje": "Informacion de propiedad - " + clave + '\n\n' + document.getElementById('cmensaje').value,
                    "telefono": document.getElementById('cphone').value,
                    "email": document.getElementById('cmail').value
                },
                success: function (data) {
                    console.log("Se envió correo");
                    $("#frmMasInfo").validate().resetForm();
                    $("#frmMasInfo")[0].reset();
                }
            })
                .fail(function (data) {
                    console.log("Por le momento no se puede enviar correo");
                    $("#frmMasInfo").validate().resetForm();
                    $("#frmMasInfo")[0].reset();
                });
        },
        Init: function () {
            var btnSend = document.getElementById("btnSendInfo");
            btnSend.addEventListener("click", Detalle.ModalSend);
            Detalle.Get();
            Detalle.GetImages();
            Detalle.ValidarMasInfo();
            Detalle.Validar();
            $("#modal-send-info").on("hide.bs.modal", function () {
                $("#frmSend").validate().resetForm();
                $("#frmSend")[0].reset();
            });
        }
    }
})();

(function () {
    $.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
    });
    Detalle.Init();
})();