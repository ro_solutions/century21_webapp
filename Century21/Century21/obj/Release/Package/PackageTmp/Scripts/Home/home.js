﻿var Home = (function () {
    var server = 'https://centurybe.rosolutions.com.mx';
    serverImg = server + "/images/";
    var contrato = 1;//Tipo de contrato. 0 = Venta, 1 = Renta
    return {
        GetAsesorMes: function () {
            $.ajax({
                type: "GET",
                url: server + "/users/search/",
                data: {
                    'userofmonth':1
                },
                success: function (data) {
                    console.log(data, "asesor Mes");
                    if (data.rows.length>0) {
                        Home.LoadAsesorMes(data.rows[0]);
                    }
                    
                }
            })
                .fail(function (data) {
                    console.error("Fail to get Asesor del mes", data);
                });
        },
        GetPropiedadesMes: function () {
            $.ajax({
                type: "GET",
                url: server + "/properties/search",
                data: {
                    propofmonth:1
                },
                success: function (data) {
                    console.log("propiedades del mes", data);
                    Home.LoadPropiedades(data.rows);
                }
            });
        },
        LoadAsesorMes: function (asesor) {
            var tel = asesor.telefono;
            document.getElementById("AseName").innerHTML ='<i class="fas fa-user"></i> '+ asesor.nombres + " " + asesor.apellidos; 
            document.getElementById("AseTel").innerHTML = '<i class="fas fa-mobile"></i> (' +
                tel[0] + tel[1] + tel[2] + ')' + tel[3] + tel[4] + tel[5] + ' - ' + tel[6] + tel[7] + tel[8] + tel[9];
                //asesor.telefono;
            document.getElementById("AseEmail").innerHTML = '<i class="fas fa-envelope"></i> '+asesor.email;
            if (asesor.image != null) {
                var url = server + '/images/' + asesor.image;
                $("#AseImg").attr("src", url);
            }
            
        },
        LoadPropiedades: function (json) {
            var html = '';



            for (var i = 0; i < json.length; i++) {
                var p = json[i];
                var tel = p.user.telefono;
                var estancia = '<div class="spf-text"><i class="fas fa-times"></i></div>';
                if (p.estancia) {
                    estancia = '<div class="spf-text"><i class="fas fa-check"></i></div>';
                }
                //console.log(p.user);
                html += '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 element">' +
                    '<div class="border">' +
                    '<div class="img-prop">' +
                    '<img src="' + (serverImg + p.property_images[0].image) + '" alt="" class="img-mes" alt="Alternate Text" />' +
                    '<div class="top-left"><img class="img-over" src="/Content/imgs/Logo_Century_Fondo.png" alt="Alternate Text" /></div>' +
                    '</div>' +
                    '<div class="sect-direction ovf-y">' +
                    '<div class="clave"><strong>Clave: ' + p.clave + '</strong></div>' +
                    '<div>' + p.direccionCalle + ' No Ext. ' + p.direccionExterior + '</div>' +
                    '<div>' + p.ciudad + '</div>' +
                    '</div>' +
                    '<div class="costo-tel ovf-y">' +
                    '<div class="col-lg-4 col-md-4 img-century-prop no-pd-L">' +
                    '<img src="/Content/imgs/Logo_Century_Fondo.png" width="100" height="70" alt="Alternate Text" />' +
                    '</div>' +
                    '<div class="col-lg-8 col-md-8 col-xs-12 tar">' +
                    '<div class="price-prop">' +
                    '$' + parseFloat(p.precio).format(2) +
                    '<div>MXN</div>' +
                    '</div>' +
                    '<div class="bold">' +
                    '<i class="fas fa-mobile"></i>(' + tel[0] + tel[1] + tel[2] + ') ' + tel[3] + tel[4] + tel[5] + ' - ' + tel[6] + tel[7] + tel[8] + tel[9]+
                    //'<i class="fas fa-mobile"></i>' + p.user.telefono+ 

                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="specifications col-lg-12 ovf-y no-pd-LR">' +
                    '<div class="col-lg-2 col-xs-2 no-pd-L tac">' +
                    '<div class="div-rounded"><i class="fas fa-square"></i></div>' +
                    '<div class="spf-text">' + parseFloat(p.terreno).format(2) + '</div>' +
                    '<div class="spf-text">m<sup>2</sup></div>'+
                    '</div>' +
                    '<div class="col-lg-2  col-xs-2 no-pd-L tac">' +
                    '<div class="div-rounded"><i class="fas fa-home"></i></div>' +
                    '<div class="spf-text">' + p.construccion + '</div>' +
                    '<div class="spf-text">m<sup>2</sup></div>'+
                    '</div>' +
                    '<div class="col-lg-2  col-xs-2 no-pd-L tac">' +
                    '<div class="div-rounded"><i class="fas fa-bed"></i></div>' +
                    '<div class="spf-text">' + p.recamaras + '</div>' +
                    '</div>' +
                    '<div class="col-lg-2  col-xs-2 no-pd-L tac">' +
                    '<div class="div-rounded"><i class="fas fa-bath"></i></div>' +
                    '<div class="spf-text">' + p.bathrooms + '</div>' +
                    '</div>' +
                    '<div class="col-lg-2  col-xs-2 no-pd-L tac">' +
                    '<div class="div-rounded"><i class="fas fa-car"></i></div>' +
                    '<div class="spf-text">' + p.cochera + '</div>' +
                    '</div>' +
                    '<div class="col-lg-2  col-xs-2 no-pd-L tac">' +
                    '<div class="div-rounded"><i class="fas fa-tv"></i></div>' +
                    estancia +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }
            $("#propMes").empty().append(html);
        },
        ValidarContacto: function () {
            $("#frmContacto").validate({
                rules: {
                    inpName: { required: true},
                    inpEmail: { required: true, email: true },
                    inpMessage: {required: true}
                },
                messages: {
                    inpName: { required: "Campo Requerido" },
                    inpEmail: { required: "Campo Requerido", email: "Ingrese Email válido" },
                    inpMessage: { required: "Campo Requerido" }
                },
                submitHandler: function (form) {
                    Home.SendMail();
                    return false;
                }
            });
        },
        Search: function () {
            window.location.href = "/Home/Resultados/?venta=" + contrato +
                "&categoria=" + $("#inpTipo").val() +
                "&clave=" + document.getElementById("inpClave").value;
            
        },
        SetVentaRenta: function (type) {
            //btn-warning
            contrato = type;
            if (contrato==1) {//Compra
                $("#btn-compra").addClass("btn-warning");
                $("#btn-compra").removeClass("btn-default");
                $("#btn-compra").removeClass("fantom");

                $("#btn-renta").removeClass("btn-warning");
                $("#btn-renta").addClass("btn-default");
                $("#btn-renta").addClass("fantom");
            }
            else if (contrato == 0) {//Renta
                $("#btn-renta").addClass("btn-warning");
                $("#btn-renta").removeClass("btn-default");
                $("#btn-renta").removeClass("fantom");

                $("#btn-compra").removeClass("btn-warning");
                $("#btn-compra").addClass("btn-default");
                $("#btn-compra").addClass("fantom");
            }
            else {
                console.error("El tipo de contrato selecciondo no existe");
            }
        },
        SendMail: function () {
            $.ajax({
                type: "",
                url: server + "",
                data: {},
                success: function () {

                }

            })
                .fail(function (data) {

                })

            $("#formAsesor").validate().resetForm();
            $("#formAsesor")[0].reset();
        },
        Init: function () {
            Home.ValidarContacto();
            Home.GetAsesorMes();
            Home.GetPropiedadesMes();
            var btnCompra = document.getElementById("btn-compra"),
                btnRenta = document.getElementById("btn-renta"),
                btnSearch = document.getElementById("btn-search");
            btnCompra.addEventListener("click", function () {
                Home.SetVentaRenta(1);
            });
            btnRenta.addEventListener("click", function () {
                Home.SetVentaRenta(0);
            });
            btnSearch.addEventListener("click", Home.Search);
        }
    }
})();

(function () {
    Home.Init();
    $.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
    });
})();