﻿function isNull(value, val) {
    return value == null ? val : value;
}
function GetValue(id) {
    return document.getElementById(id).value;
};
function SetValue(id, value) {
    return document.getElementById(id).value = value ;
};
var Clientes = (function () {
    var server = 'https://centurybe.rosolutions.com.mx',
        xToken = xTok,
        objFull = [],
        count = 0, pkId = null;

    return {
        //Obtiene la lista de clientes
        
        GetClientes: function () {
            var filtro = document.getElementById("inpFilterEmail").value;
            console.log("get", filtro);
            
            $.ajax({
                type: "GET",
                url: server + "/clients/search/",
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "search": filtro
                },
                success: function (data) {
                    //console.log("success", data);
                    objFull = data.rows;
                    Clientes.LoadTable(objFull);
                }
            }).fail(function (data) {
                console.log("fail qpd", data);
            });
        },
        //Carga la tabla principal con los datos obtenidos en GetClientes
        LoadTable: function (json) {
            objFull = json;
            $("#tblClientes").DataTable({
                destroy: true,
                info: false,
                searching: true,
                "paging": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "aaData": json,
                "aoColumns": [
                    {
                        "mRender": function (data, type, full) {
                            return full.nombres + " " + full.apellidos;
                        }
                    },
                    { "mData": "empresa" },
                    {
                        "mRender": function (data, type, full) {
                            var telefono = '<div><strong>Casa: </strong> ' + isNull(full.telefono_casa, "") + '</div>' +
                                '<div> <strong>Oficina: </strong>' + isNull(full.telefono_oficina, "") + '</div>' +
                                '<div> <strong>Cel: </strong>' + isNull(full.telefono_celular, "") + '</div>';

                            return telefono;
                        }
                    },
                    {
                        "mRender": function (data, type, full) {
                            var direccion = '<div class="col-lg-12 col-xs-12"><strong> Calle: </strong>' + isNull(full.direccionCalle, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>Col: </strong>' + isNull(full.direccionColonia, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>Num Ext: </strong>' + isNull(full.direccionExterior, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>Num Int: </strong>' + isNull(full.direccionInterior, "") + '</div>' +
                                '<div class="col-lg-12 col-xs-12"><strong>CP: </strong>' + isNull(full.cp, "") + '</div>';

                            return direccion;
                        }
                    },
                    { "mData": "email" },
                    {
                        "mRender": function (data, type, full) {
                            var idx = objFull.indexOf(full);
                            return '<button class="btn btn-info" onClick="Clientes.modalEdit(' + idx + ')"><i class="fas fa-edit"></i></button>';
                        }
                    }
                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "zeroRecords": "No se encontró nada - lo sentimos",
                    "search": "Buscar en tabla:",
                    "emptyTable": "No hay datos disponibles",
                    "infoEmpty": "No se encuentra información disponible",
                    "infoFiltered": "(filtrado de _MAX_ items totales)",
                    "paginate":
                    {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        },
        //Valida campos antes de agregar o editar una propiedad
        Validar: function () {
            $("#formCliente").validate({
                focusCleanup: true,
                rules: {
                    nameCliente: { required: true },
                    apellidosCliente: { required: true },
                    inpCiudad: { required: false },
                    inpEstado: { required: false },
                    inpCalle: { required: true },
                    inpColonia: { required: true },
                    inpInterior: { required: false },
                    inpExterior: { required: true },
                    inpCP: { required: false },
                    email: { required: true },
                    inpEmpresa: { required: false },
                    telCasa: { required: false },
                    telOficina: { required: false },
                    telCel: { required: false },
                    inpComentarios: { required: false }
                },
                messages: {
                    nameCliente: { required: "Campo Requerido" },
                    apellidosCliente: { required: "Campo Requerido" },
                    inpCiudad: { required: "Campo Requerido" },
                    inpEstado: { required: "Campo no Requerido" },
                    inpCalle: { required: "Campo Requerido" },
                    inpColonia: { required: "Campo Requerido" },
                    inpInterior: { required: "Campo Requerido" },
                    inpExterior: { required: "Campo Requerido" },
                    inpCP: { required: "Campo Requerido" },
                    email: { required: "Campo Requerido" },
                    inpEmpresa: { required: "Campo Requerido" },
                    telCasa: { required: "Campo Requerido" },
                    telOficina: { required: "Campo Requerido" },
                    telCel: { required: "Campo Requerido" },
                    inpComentarios: { required: "Campo Requerido" }
                },
                submitHandler: function (form) {
                    console.log("submit", form);
                    objAction[action]();
                    return false;
                }
            });
        },
        //POST para agregar una propiedad
        Add: function myfunction() {
            console.log("add cliente");
            $.ajax({
                type: "POST",
                url: server + "/clients/",
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "nombres": GetValue("nameCliente"),
                    "apellidos": GetValue("apellidosCliente"),
                    "direccionCalle": GetValue("inpCalle"),
                    "direccionColonia": GetValue("inpColonia"),
                    "direccionExterior": GetValue("inpExterior"),
                    "direccionInterior": GetValue("inpInterior"),
                    "cp": GetValue("inpCP"),
                    "ciudad": GetValue("inpCiudad"),
                    "estado": $("#inpEstado").select2('val'),
                    "email": GetValue("email"),
                    "empresa": GetValue("inpEmpresa"),
                    "telefono_casa": GetValue("telCasa"),
                    "telefono_celular": GetValue("telCel"),
                    "telefono_oficina": GetValue("telOficina"),
                    "comentarios": GetValue("inpComentarios")
                },
                success: function (data) {
                    console.log("succ", data);
                    Clientes.GetClientes();
                    $(".modal").modal("hide");
                },
                fail: function (data) {
                    console.log("fail", data);
                }
            }).fail(function (data) {
                console.log("fail", data);
            });
        },
        //POST para editar una propiedad
        Edit: function () {
            console.log("edit cliente");
            $.ajax({
                type: "PUT",
                url: server + "/clients/" + pkId,
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "nombres": GetValue("nameCliente"),
                    "apellidos": GetValue("apellidosCliente"),
                    "direccionCalle": GetValue("inpCalle"),
                    "direccionColonia": GetValue("inpColonia"),
                    "direccionExterior": GetValue("inpExterior"),
                    "direccionInterior": GetValue("inpInterior"),
                    "cp": GetValue("inpCP"),
                    "ciudad": GetValue("inpCiudad"),
                    "estado": $("#inpEstado").select2('val'),
                    "email": GetValue("email"),
                    "empresa": GetValue("inpEmpresa"),
                    "telefono_casa": GetValue("telCasa"),
                    "telefono_celular": GetValue("telCel"),
                    "telefono_oficina": GetValue("telOficina"),
                    "comentarios": GetValue("inpComentarios")
                },
                success: function (data) {
                    //Llamar notifications
                    Clientes.GetClientes();
                    $(".modal").modal("hide");
                },
                fail: function (data) {
                    console.log("fail", data);
                }
            }).fail(function (data) {
                console.log("fail", data);
            });
        },
        // abre el modal para agregar Clientes
        modalAdd: function () {
            action = actions[0];
            document.getElementById('titleCliente').innerText = 'Agregar Cliente';
            document.getElementById('btnModalAdd').innerText= "Agregar";
            $("#modal-add").modal("show");
            document.getElementById("nameCliente").focus();
        },
        //Carga los datos del cliente en el modal y lo muestra
        modalEdit: function (idx) {
            var cliente = objFull[idx];
            pkId = objFull[idx].client_id;
            SetValue("nameCliente", cliente.nombres);
            SetValue("apellidosCliente", cliente.apellidos);
            SetValue("inpCiudad", cliente.ciudad);
            $("#inpEstado").val(cliente.estado).trigger('change.select2');
            SetValue("inpCalle", cliente.direccionCalle);
            SetValue("inpColonia", cliente.direccionColonia);
            SetValue("inpInterior", cliente.direccionInterior);
            SetValue("inpExterior", cliente.direccionExterior);
            SetValue("inpCP", cliente.cp);
            SetValue("email", cliente.email);
            SetValue("inpEmpresa", cliente.empresa);
            SetValue("telCasa", cliente.telefono_casa);
            SetValue("telOficina", cliente.telefono_oficina);
            SetValue("telCel", cliente.telefono_celular);
            SetValue("inpComentarios", cliente.comentarios);

            console.log(cliente);
            action = actions[1];
            document.getElementById('titleCliente').innerText = 'Editar Cliente';
            document.getElementById('btnModalAdd').innerText = "Agregar";
            $("#modal-add").modal("show");
            document.getElementById("nameCliente").focus();
        },
        //Inicializa los componentes
        Init: function () {
            $("#inpEstado").select2({
                width: 'resolve',
                placeholder: 'Ingrese Estado',
                allowClear: true,
            });
            Clientes.Validar();
            Clientes.GetClientes();
            var modal = document.getElementById("btnAdd"),
                btnBusqueda = document.getElementById("filterBtn");
            modal.addEventListener("click", Clientes.modalAdd);
            btnBusqueda.addEventListener("click", Clientes.GetClientes);
            $("#modal-add").on("show.bs.modal", function () {
                $("#nameCliente").focus();
            });

            $("#modal-add").on("hide.bs.modal", function () {
                $("#formCliente").validate().resetForm();
                $("#formCliente")[0].reset();
            });
        }

    }
})();

const actions = ["add", "edit"];
var action = actions[0], objAction = { "add": Clientes.Add, "edit": Clientes.Edit };
(function () {
    Clientes.Init();
})();