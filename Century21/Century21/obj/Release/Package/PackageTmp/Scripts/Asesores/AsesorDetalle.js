﻿function isNull(value, val) {
    return value == null ? val : value;
}

var Asesores = (function () {
    var server = 'https://centurybe.rosolutions.com.mx',
        objFull = [];

    return {
        Get: function () {

            $.ajax({
                type: "GET",
                url: server + "/users/search/",
                data: {
                    "type": 2
                },
                success: function (data) {
                    objFull = data.rows;
                    console.log("GET-ASESORES", data.rows);
                    Asesores.LoadTable(objFull);
                    $('[data-toggle="tooltip"]').tooltip();
                }
            })
                .fail(function (data) {
                    console.log("done", data);
                });
        },
        LoadTable: function (asesores) {
            $("#tblAsesores").DataTable({
                destroy: true,
                info: false,
                searching: true,
                "paging": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "aaData": asesores,
                "aoColumns": [
                    {
                        "mRender": function (data, type, full) {
                            return full.nombres + " " + full.apellidos;
                        }
                    },
                    { "mData": "email" },
                    { "mData": "telefono" },
                    {
                        "mRender": function (data, type, full) {
                            var idx = objFull.indexOf(full);
                            return '<button title="PROPIEDADES DEL ASESOR" data-toggle="tooltip" data-placement="left" class="btn btn-warning" onclick="Asesores.OpenModalDetalle(' + idx + ');"><i class="fas fa-list"></i></button> ';
                        }, "sClass": "tar"
                    }
                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "zeroRecords": "No se encontró nada - lo sentimos",
                    "search": "Buscar en tabla:",
                    "emptyTable": "No hay datos disponibles",
                    "infoEmpty": "No se encuentra información disponible",
                    "infoFiltered": "(filtrado de _MAX_ items totales)",
                    "paginate":
                    {
                        "next": '<i class="fas fa-angle-double-right"></i>',
                        "previous": '<i class="fas fa-angle-double-left"></i>'
                    }
                }
            });
        },
        OpenModalDetalle: function (idx) {
            var asesor = objFull[idx];
            Asesores.SetImgAsesor(asesor);
            Asesores.GetDetalle(asesor.user_id);
            $("#modal-detalle").modal("show");
        },
        SetImgAsesor: function (asesor) {
            document.getElementById("AseName").innerHTML = '<i class="fas fa-user"></i> ' + asesor.nombres + " " + asesor.apellidos;
            document.getElementById("AseTel").innerHTML = '<i class="fas fa-mobile"></i> <label class="txt-yellow">' +  asesor.telefono+'</label>';
            document.getElementById("AseEmail").innerHTML = '<i class="fas fa-envelope"></i> ' + asesor.email;

            var url = server + '/images/' + asesor.image;
            $("#imgAsesor").attr("src", url);
        },
        GetDetalle: function (pkId) {
            $.ajax({
                type: "GET",
                url: server + "/properties/search",
                data: {
                    user: pkId
                },
                success: function (data) {
                    console.log(data);
                    Asesores.LoadTableDetalle(data.rows);
                }
            })
                .fail(function (error) {
                    console.error("fail", error.message);
                })
        },

        LoadTableDetalle: function (propiedades) {
            $("#tblDetalle").DataTable({
                destroy: true,
                info: false,
                searching: true,
                "lengthMenu": [5, 10, 15, 20],
                "paging": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "aaData": propiedades,
                "aoColumns": [
                    { "mData": "clave", "sClass": "cell-gray txt-yellow tac bold" },
                    {
                        //dirección Ubicación
                        "mRender": function (data, type, full) {
                            var direccion = isNull(full.direccionCalle, "") + ' ' + isNull(full.direccionExterior, " S/N");

                            return direccion;
                        }, "sClass": "cell-gray"
                    },
                    { "mData": "zona", "sClass": "cell-gray" },
                    {
                        "mData": "precio",
                        "mRender": function (data) {
                            return "$ " + parseFloat(data).format(2);
                        }, "sClass": "cell-gray bold tar"
                    }
                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "zeroRecords": "No se encontró nada - lo sentimos",
                    "search": "Buscar en tabla:",
                    "emptyTable": "No hay datos disponibles",
                    "infoEmpty": "No se encuentra información disponible",
                    "infoFiltered": "(filtrado de _MAX_ items totales)",
                    "paginate":
                    {
                        "next": '<i class="fas fa-angle-double-right"></i>',
                        "previous": '<i class="fas fa-angle-double-left"></i>'
                    }
                }
            });
        }
    }
})();

(function () {
    Asesores.Get();
})();