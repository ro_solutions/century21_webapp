﻿function isNull(value, val) {
    return value == null ? val : value;
}
function GetValue(id) {
    return document.getElementById(id).value;
};
function SetValue(id, value) {
    return document.getElementById(id).value = value;
};
var Asesores = (function () {
    //puede ser add o edit para mandar llamar a la funcion correcta
    var server = 'https://centurybe.rosolutions.com.mx',
        xToken = xTok,
        objFull = [],
        count = 0, pkId = null;

    return {
        GetAsesores: function () {
            var filtro = document.getElementById("inpFilterEmail").value;
            $.ajax({
                type: "GET",
                url: server + "/users/search/",
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "search": filtro,
                    "type": 1
                },
                success: function (data) {
                    objFull = data.rows;
                    console.log("GET-ASESORES", data.rows);
                    Asesores.LoadTable(objFull);
                    StopLoader('.contenedor');
                },
                fail: function (data) {
                    console.log("done", data);
                    StopLoader('.contenedor');
                }
            });
        },
        LoadTable: function (json) {
            objFull = json;
            $("#tblAsesores").DataTable({
                destroy: true,
                info: false,
                searching: true,
                "paging": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "aaData": json,
                "aoColumns": [
                    { "mData": "user" },
                    {
                        "mRender": function (data, type, full) {
                            return full.nombres + " " + full.apellidos;
                        }
                    },
                    { "mData": "email" },
                    {
                        "mData": "type",
                        "mRender": function (data) {
                            if (data == 1) {
                                return "Administrador";
                            }
                            else {
                                return "Asesor";
                            }
                        }
                    },
                    {
                        "mData": "status",
                        "mRender": function (data) {
                            if (data) {
                                return "Activo";
                            }
                            else {
                                return "Inactivo";
                            }
                        }
                    },
                    {
                        "mData": "asesorMes",
                        "mRender": function (data, type, full) {
                            var idx = objFull.indexOf(full);
                            if (full.type == 2) {
                                if (data == true) {
                                    return ' <button title="ASESOR DEL MES" class="btn btn-success"><i class="fas fa-check"></i></button>';
                                }
                                else {
                                    return ' <button title="Asignar como Asesor del mes" class="btn btn-success success-color btn-control" onclick="Asesores.SetMes(' + idx + ');"><i class="fas fa-check"></i></button>'
                                }
                            }
                            else {
                                return '<button class="btn btn-info" title="Solo Asesores, este usuario es administrador"><i class="fas fa-info"></i></button>';
                            }
                            
                        }, "sClass": "tac"
                    },
                    {
                        "mRender": function (data, type, full) {
                            var idx = objFull.indexOf(full);
                            //asesorMes
                            return '<button title="Editar Datos" class="btn btn-info" onclick="Asesores.modalEdit(' + idx + ');"><i class="fas fa-edit"></i></button> ' +
                                ' <button title="Cambiar imagen de perfil" class="btn btn-success" onclick="Asesores.modalProfilePicture(' + idx + ');"><i class="fas fa-camera"></i></button>';
                        }, "sClass": "tar"
                    }
                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "zeroRecords": "No se encontró nada - lo sentimos",
                    "search": "Buscar en tabla:",
                    "emptyTable": "No hay datos disponibles",
                    "infoEmpty": "No se encuentra información disponible",
                    "infoFiltered": "(filtrado de _MAX_ items totales)",
                    "paginate":
                    {
                        "next": '<i class="fas fa-angle-double-right"></i>',
                        "previous": '<i class="fas fa-angle-double-left"></i>'
                    }
                }
            });
        },
        //Valida campos antes de agregar o editar una propiedad
        Validar: function myfunction() {
            $("#formAsesor").validate({
                focusCleanup: true,
                rules: {
                    nameAsesor: { required: false },
                    apellidoAsesor: { required: false },
                    userName: { required: true },
                    email: { required: true },
                    pswd: { required: true },
                    confPswd: { required: true, equalTo: "#pswd" },
                    userType: { required: true },
                    telephone: { required: false },
                    //telephone: { required: false },
                    initDate: { required: false },
                    status: { required: true}
                },
                messages: {
                    nameAsesor: { required: "Campo no Requerido" },
                    apellidoAsesor: { required: "Campo no Requerido" },
                    userName: { required: "Campo Requerido" },
                    email: { required: "Campo Requerido" },
                    pswd: { required: "Campo Requerido" },
                    confPswd: {
                        required: "Campo Requerido",
                        equalTo: "Las contraseñas no coinciden"
                    },
                    userType: { required: "Campo Requerido" },
                    telephone: { required: "Campo Requerido" },
                    //telephone: { required: "Campo Requerido" },
                    initDate: { required: "Campo Requerido" },
                },
                submitHandler: function (form) {
                    objAction[action]();
                    return false;
                }
            });
        },
        //POST para agregar una propiedad
        Add: function myfunction() {
            console.log({
                "user": GetValue("userName"),
                "password": GetValue("pswd"),
                "email": GetValue("email"),
                //"type": parseInt($("#userType").val()),
                "type":1,
                "status": $("#status").val(),
                "nombres": GetValue("nameAsesor"),
                "apellidos": GetValue("apellidoAsesor"),
                "telefono": GetValue("telephone"),
                //"startDate": new Date().toString()
            });
            $.ajax({
                type: "POST",
                url: server + "/users/",
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "user": GetValue("userName"),
                    "password": GetValue("pswd"),
                    "email": GetValue("email"),
                    "type": parseInt($("#userType").val()),
                    "status": $("#status").val(),
                    "nombres": GetValue("nameAsesor"),
                    "apellidos": GetValue("apellidoAsesor"),
                    "telefono": GetValue("telephone"),
                    //"startDate": new Date().toString()
                },
                success: function (data) {
                    console.log("succ");
                    Asesores.GetAsesores();
                    $(".modal").modal("hide");
                }
            }).fail(function (data) {
                console.log("fail", data);
            });
        },
        //POST para editar una propiedad
        Edit: function () {
            var data = null;
            var valuePsw = GetValue("pswd");
            if (valuePsw == "" || valuePsw == "undefined") {
                data = {
                    "user": GetValue("userName"),
                    "email": GetValue("email"),
                    "type": parseInt($("#userType").val()),
                    "status": $("#status").val(),
                    "nombres": GetValue("nameAsesor"),
                    "apellidos": GetValue("apellidoAsesor"),
                    "telefono": GetValue("telephone")
                }
            }
            else {
                data = {
                    "user": GetValue("userName"),
                    "password": GetValue("pswd"),
                    "email": GetValue("email"),
                    "type": parseInt($("#userType").val()),
                    "status": $("#status").val(),
                    "nombres": GetValue("nameAsesor"),
                    "apellidos": GetValue("apellidoAsesor"),
                    "telefono": GetValue("telephone")
                }
            }

            $.ajax({
                type: "PUT",
                url: server + "/users/" + pkId,
                headers: {
                    "X-Api-Token": xToken
                },
                data: data,
                success: function (data) {
                    console.log("succ");
                    Asesores.GetAsesores();
                    $(".modal").modal("hide");
                }
            }).fail(function (data) {
                console.log("fail", data.responseJSON.errors);
            });
        },
        //Cambiar Imagen de perfil del asesor
        SetProfilePicture: function () {
            console.log("set picture put");
            var dataToSend = new FormData();
            
            dataToSend.append('image', $('#inpImgProfile')[0].files[0]);
            
            var url = server + "/users/" + pkId + "/image";
            console.log("url",url);
            $.ajax({
                method: "PUT",
                url: url,
                headers: {
                    "X-Api-Token": xToken,
                    
                },
                contentType: false,
                processData: false,
                data: dataToSend,
                success: function (data) {
                    console.log("success profile image");
                    $(".modal").modal("hide");
                    Asesores.GetAsesores();
                }
            })
                .fail(function (data) {
                    console.log("fail", data);
                });
        },
        SetMes: function (idx) {
            console.log('cambiar asesor mes');
            var asesor = objFull[idx];
            $.ajax({
                type: "PUT",
                url: server + "/users/" + asesor.user_id,
                headers: {
                    "X-Api-Token": xToken
                },
                data: {
                    "asesorMes": true
                },
                success: function (data) {
                    console.log("asesor del mes Exito!");
                    Asesores.GetAsesores();
                }
            })
                .fail(function (data) {
                    console.error("fail ");
                });
        },
        // abre el modal para agregar Asesores
        modalAdd: function () {
            action = actions[0];
            document.getElementById('titleAsesor').innerText = 'Agregar Asesor';
            document.getElementById('btnModalAdd').innerText = "Agregar";
            $("#modal-add").modal("show");
            document.getElementById("nameAsesor").focus();
        },
        //modal para editar imagen 
        modalProfilePicture: function (idx) {
            var asesor = objFull[idx];
            pkId = asesor.user_id;
            if (asesor.image != null) {
                var url = server + '/images/' + asesor.image;
                $("#imgResult").attr("src", url);
            }
            else {
                $("#imgResult").attr("src", "/Content/imgs/perfil-silueta-usuario_318-40557.jpg");
            }
            
            console.log("editar imagen",asesor, url);
            //renderizar imagen 
            $("#modal-img").modal("show");
        },
        //asigna los valores a los inputs de modal correspondientes y abre el modal add
        modalEdit: function (idx) {
            var asesor = objFull[idx];
            console.log(asesor, "asesor");
            pkId = asesor.user_id;

            SetValue("nameAsesor", asesor.nombres);
            SetValue("apellidoAsesor", asesor.apellidos);
            SetValue("userName", asesor.user);
            SetValue("email", asesor.email);
            SetValue("pswd", "");
            SetValue("confPswd", "");
            SetValue("telephone", asesor.telefono);

            $("#userType").val(asesor.type);
            $("#status").val(asesor.status ? "true" : "false");

            action = actions[1];
            document.getElementById('titleAsesor').innerText = 'Editar Asesor';
            document.getElementById('btnModalAdd').innerText = "Editar";



            $("#status").removeClass("hide");
            $("#modal-add").modal("show");
            //document.getElementById("nameAsesor").focus();
            //falta poner oculta la casilla de confirmar contraseña
        },
        //inicializa los componentes
        Init: function () {
            RunLoader(".contenedor");
            //Iniciar input tipo asesor y status
            Asesores.Validar();
            Asesores.GetAsesores();

            var modal = document.getElementById("btnAdd"),
                btnBusqueda = document.getElementById("filterBtn"),
                btnImg = document.getElementById("btnChangeImg");
            modal.addEventListener("click", Asesores.modalAdd);
            btnBusqueda.addEventListener("click", Asesores.GetAsesores);
            btnImg.addEventListener("click", Asesores.SetProfilePicture);
            $("#modal-add").on("show.bs.modal", function () {
                $("#nameAsesor").focus();
            });

            $("#modal-add").on("hide.bs.modal", function () {

                $("#formAsesor").validate().resetForm();
                $("#formAsesor")[0].reset();
            });
        }

    }
})();

const actions = ["add", "edit"];
var action = actions[0], objAction = { "add": Asesores.Add, "edit": Asesores.Edit };

(function () {
    Asesores.Init();
})();



function ReadImg(evt) {
    console.log("archivo reader");
    var files = evt.target.files; // FileList object

    //Obtenemos la imagen del campo "file". 
    for (var i = 0, f; f = files[i]; i++) {
        //Solo admitimos imágenes.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        reader.onload = (function (theFile) {
            //clousure
            return function (e) {
                console.log("onload");
                // Creamos la imagen.
                //document.getElementById("list").innerHTML =
                //    ['<img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"/>'].join('');
                $("#imgResult").attr("src", e.target.result);
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

document.getElementById('inpImgProfile').addEventListener('change', ReadImg, false);
