﻿
var Resultados = (function () {
    var server = 'https://centurybe.rosolutions.com.mx';
    var serverImg = server + "/images/";
    var page = 1,
        limite = 10,
        totalResults = 0,
        imgP = null,
        _arr=[];
    return {
        //Realiza la búsqueda para obtener json de las propiedades
        GetPropiedades: function () {

            var objData = { clave: clave, limit: limite, page: page }

            if (venta.length > 0) {
                objData.contract = parseInt(venta);
            }

            if (categoria != '' && categoria != null ) {
                objData.type = parseInt( categoria);
            }

            console.log("data para buscar", objData);
            $.ajax({
                type: "GET",
                url: server + "/properties/search",
                data: objData,
                success: function (data) {
                    //console.log("esta buscando", data);
                    if (data.rows.length > 0) {
                        $("#noResults").addClass("hide");
                        //Resultados.GetImg(data.rows);
                        Resultados.SetPagin(data.count);
                        Resultados.FillResults(data.rows);
                    }
                    else {
                        //Mensaje de no hay resultados
                        $("#noResults").removeClass("hide");
                        Resultados.SetPagin(data.count);
                        Resultados.FillResults(data.rows);
                    }
                }
            })
                .fail(function (data) {
                    console.error("falló la búsqueda", data);
                });
        },
        SetUser: function () {

        },
        //recibe Json con resultados y los pone en el contenedor results
        FillResults: function (json) {
            
            var html = '';

            for (var i = 0; i < json.length; i++) {
                var p = json[i];
                
                var estancia = '<div class="spf-text"><i class="fas fa-times"></i></div>';
                if (p.estancia) {
                    estancia = '<div class="spf-text"><i class="fas fa-check"></i></div>';
                }
                //console.log(p.user);
                html += '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 element" onclick="Resultados.Detalle('+p.property_id+')" style="cursor:pointer;">' +
                    '<div class="border">' +
                    '<div class="img-prop">' +
                    '<img src="' + (serverImg + p.property_images[0].image)+'" alt="" class="img-mes" alt="Alternate Text" />' +
                        '<div class="top-left"><img class="img-over" src="/Content/imgs/Logo_Century_Fondo.png" alt="Alternate Text" /></div>'+
                    '</div>' +
                    '<div class="sect-direction ovf-y">' +
                        '<div class="clave"><strong>Clave: ' + p.clave + '</strong></div>'+
                            '<div>' + p.direccionCalle + ' No Ext. ' + p.direccionExterior+'</div>'+
                            '<div>' + p.ciudad +'</div>'+
                        '</div>'+
                        '<div class="costo-tel ovf-y">'+
                            '<div class="col-lg-4 col-md-4 no-pd-L img-century-prop">'+
                                '<img src="/Content/imgs/Logo_Century_Fondo.png" width="100" height="70" alt="Alternate Text" />'+
                            '</div>'+
                            '<div class="col-lg-8 tar">'+
                                '<div class="price-prop">'+
                                    '$' + parseFloat(p.precio).format(2) +
                                '</div>'+
                                '<div class="bold">MXN'+
                                '</div>'+
                                '<div class="bold">'+
                                    '<i class="fas fa-mobile"></i>'+ p.user.telefono+
                        '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="specifications col-lg-12 ovf-y no-pd-LR">'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pd-L tac">'+
                                '<div class="div-rounded"><i class="fas fa-square"></i></div>'+
                                '<div class="spf-text">' + parseFloat(p.terreno).format(2) + '</div>' +
                                '<div class="spf-text">m<sup>2</sup></div>' +
                            '</div>'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pd-L tac">'+
                                '<div class="div-rounded"><i class="fas fa-home"></i></div>'+
                                '<div class="spf-text">' + p.construccion + '</div>' +
                                '<div class="spf-text">m<sup>2</sup></div>' +
                            '</div>'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pd-L tac">'+
                                '<div class="div-rounded"><i class="fas fa-bed"></i></div>'+
                                '<div class="spf-text">' + p.recamaras+'</div>'+
                            '</div>'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pd-L tac">'+
                                '<div class="div-rounded"><i class="fas fa-bath"></i></div>'+
                                '<div class="spf-text">' + p.bathrooms+'</div>'+
                            '</div>'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pd-L tac">'+
                                '<div class="div-rounded"><i class="fas fa-car"></i></div>'+
                                '<div class="spf-text">' + p.cochera+'</div>'+
                            '</div>'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-pd-L tac">'+
                                '<div class="div-rounded"><i class="fas fa-tv"></i></div>' +
                                   estancia+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
            }
            //console.log('los resultados son 0 limpialos');
            $("#results").empty().append(html);

            //Resultados.GetImg(json);
        },
        SetPagin: function (num) {
            totalResults = num;
            //console.log("totals",totalResults);
            var pages = Math.ceil(totalResults / limite);
            //var html = '<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a ></li >';
            var html = '';
            for (var i = 1; i <= pages; i++) {
                //console.log("deberia agregar 1 al menos");
                html += '<li><a href="#" onclick="Resultados.SetPage(' + i +')">'+i+'</a></li>';
            }
            //html += '<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a ></li >';

            $(".pagination").empty().append(html);
        },
        SetPage: function (pag) {
            page = pag;
            Resultados.Search();
        },
        Search: function () {
            //set vars
            venta = $("#slctCompra").val();
            categoria =  $("#inpTipo").val();
            clave = document.getElementById('inpClave').value;
            limite = parseInt( $(slctResults).val());

            Resultados.GetPropiedades();
        },
        Detalle: function (id) {
            window.location.href = "/Home/Detalle/?pk=" + id;
        },

        Init: function () {
            $("#slctCompra").val(venta.length > 0 ? venta : 0);
            if (categoria.length > 0) {
                $("#inpTipo").val(categoria);
            }
            $("#inpClave").val(clave);

            Resultados.GetPropiedades();

            var btnFiltro = document.getElementById("btn-search"),
                slctResults = document.getElementById('slctResults');

            btnFiltro.addEventListener("click", Resultados.Search);
            slctResults.addEventListener('change', Resultados.Search);

        }
    }

})();

(function () {

    Resultados.Init();
})();