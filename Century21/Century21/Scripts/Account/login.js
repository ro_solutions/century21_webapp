﻿var IniciarSesion = function () {
    var usnm = document.getElementById("username").value,
        pswd = document.getElementById("userpswd").value,
        server = 'https://centurybe.rosolutions.com.mx',
        xToken = null;
    console.log("iniciar sesion function");
    $.post(server + "/auth/login", { "user": usnm, "password": pswd })
        .success(function (data) {
            console.log(data.user, "data.document");
            console.log("ledata", data);
            xToken = data.token;
            //data.user=  {user_id: 1, type: 1, nombres: "José Eduardo", apellidos: "Gaytán Bustamante" }
            $.post("/Account/IniciarSesion",
                {
                    token: xToken,
                    user_id: data.user.user_id, 
                    type: data.user.type, 
                    nombres: data.user.nombres, 
                    apellidos: data.user.apellidos
                })
                .success(function (data) {
                    location.href = "/Propiedades"
                });
        })
        .fail(function (data) {
            console.error(data.responseJSON.message, "fail");
        });
};

var validateForm = function () {
    $("#frm_login").validate({
        rules: {
            username: { required: true },
            userpswd: { required: true }
        },
        messages: {
            username: { required: "Campo Requerido" },
            userpswd: {required: "Campo Requerido"}
        },
        submitHandler: function (form) {
            console.log("login");
            IniciarSesion();
            return false;
        }
    });
};

(function () {
    validateForm();
    document.getElementById('username').focus();
})();
