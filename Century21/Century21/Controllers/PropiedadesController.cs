﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Century21.Controllers
{
    public class PropiedadesController : Controller
    {
        // GET: Propiedades
        public ActionResult Index()
        {
            if (Session["tok"] == null)
            {
                return Redirect("/");
            }
            else
            {
                ViewBag.tipo = Session["type"];
                return View();
            }
        }

        public ActionResult GetTipoPropiedad()
        {
            string json = "[{tipo:'Residencial'}, {tipo: 'Comercial'}, {tipo: 'Industrial'}, {tipo: 'Desarrollo'}, {tipo: 'Playas'}, {tipo: 'Campestre'}]";
            return Content(json, "text/plain");
        }

        public ActionResult GetDetallePropiedad()
        {
            string json = "";
            return Content(json, "text/plain");
        }

        [HttpPost]
        public ActionResult AddPropiedades()
        {
            string json = "";
            return Content(json, "text/plain");
        }
    }
}