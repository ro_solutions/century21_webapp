﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Century21.Controllers
{
    public class AsesoresController : Controller
    {
        // GET: Asesores
        /// <summary>
        /// vista para usuario Administrador
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            if (Session["tok"] == null)
            {
                return Redirect("/");
            }
            else
            {
                ViewBag.tok = Session["tok"].ToString();
                return View();
            }
        }
        /// <summary>
        /// vista para usuario cliente
        /// </summary>
        /// <returns></returns>
        public ActionResult Asesores()
        {
            return View();
        }
    }
}