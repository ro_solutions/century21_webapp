﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Century21.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Resultados(int? venta, int? categoria, string clave)
        {
            ViewBag.venta = venta;
            ViewBag.categoria = categoria;
            ViewBag.clave = clave;
            return View();
        }


        public ActionResult Nosotros()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Privacidad()
        {
            return View();
        }

        public ActionResult Detalle(int pk)
        {
            ViewBag.pk = pk;
            return View();
        }

    }
}