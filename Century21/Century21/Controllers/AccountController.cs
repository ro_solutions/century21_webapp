﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Century21.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogIn() {
            if (Session["tok"] != null)
            {
                return Redirect("/Propiedades");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult IniciarSesion
            (string token,int user_id, int type, string nombres, string apellidos)
        {
            Session["tok"] = token;
            Session["type"] = type;
            Session["userid"] = user_id;
            Session["nombres"] = nombres;
            Session["apellidos"] = apellidos;
            return Content("true", "text/plain");
        }

        public ActionResult CerrarSesion() {
            Session["tok"] = null;
            return Redirect("/");
        }

    }
}