﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Century21.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult AdminIdx()
        {
            return View();
        }

        public ActionResult Documentos()
        {
            if (Session["tok"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Formularios()
        {
            if (Session["tok"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}