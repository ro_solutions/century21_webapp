﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Century21.Controllers
{
    public class ClientesController : Controller
    {
        // GET: Clientes
        public ActionResult index()
        {
            if (Session["tok"] == null)
            {
                return Redirect("/");
            }
            else
            {
                ViewBag.tok = Session["tok"].ToString();
                return View();
            }
        }

    }
}